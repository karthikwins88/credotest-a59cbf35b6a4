﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web.Http;

namespace ToDoApp.Controllers
{
    public class StatusController : ApiController
    {
        // GET
        //Api/Status
        public IHttpActionResult Get()
        {
            SqlConnection conn = new SqlConnection("Data Source=localhost;Initial Catalog=ToDoApp;User Id=sa;Password=Pr0f1t;");
            SqlCommand cmd = new SqlCommand("SELECT Id AS Id, Name AS Name FROM Status ORDER BY 2;", conn, null);
            cmd.CommandType = CommandType.Text;

            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            return Ok(ds);
        }
    }
}
