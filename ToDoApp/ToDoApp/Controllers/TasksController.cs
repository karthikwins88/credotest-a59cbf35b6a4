﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Mvc;
using ToDoApp.Models;

namespace ToDoApp.Controllers
{
    public class TasksController : ApiController
    {
        readonly SqlConnection _conn = new SqlConnection("Data Source=localhost;Initial Catalog=ToDoApp;User Id=sa;Password=Pr0f1t;");
        // GET: api/Tasks/Status
        public IEnumerable<TasksModel> Get(int status = 0)
        {
            var sql = "SELECT T.Id, T.Name, S.Name FROM Tasks T JOIN Status S ON T.Status = S.Id;";
            if (status != 0) sql = "SELECT T.Id, T.Name, S.Name FROM Tasks T JOIN Status S ON T.Status = S.Id WHERE T.Status='" +
                         status + "';";
            var cmd = new SqlCommand(sql, _conn, null) {CommandType = CommandType.Text};
            var da = new SqlDataAdapter(cmd);
            var ds = new DataSet();
            da.Fill(ds);
            var tms = (from DataRow dr in ds.Tables[0].Rows
                select new TasksModel
                {
                    Id = int.Parse(dr[0].ToString()),
                    Name = dr[1].ToString(),
                    Status = dr[2].ToString()
                }).ToList();
            return tms;
        }
        
        // POST: api/Tasks/taskname/status
        public void Post(string taskName, int status = 0)
        {
            var sql = "INSERT INTO Tasks(taskName) VALUES ('" + taskName + "');";
            if (status != 0) sql = "INSERT INTO Tasks(taskName, status) VALUES ('" + taskName + "','" + status + "');";
            var cmd = new SqlCommand(sql, _conn, null) { CommandType = CommandType.Text };
            var da = new SqlDataAdapter(cmd);
        }

        // PUT: api/Tasks/5/taskname/status
        public void Put(int id, string taskName, int status = 0)
        {
            var sql = "UPDATE Tasks SET taskName = '" + taskName + "' WHERE id='" + id + "';";
            if (status != 0) sql = "UPDATE Tasks SET taskName='" + taskName + "', status='" + status + "' WHERE id='" + id + "';";
            var cmd = new SqlCommand(sql, _conn, null) { CommandType = CommandType.Text };
            var da = new SqlDataAdapter(cmd);
        }
    }
}
