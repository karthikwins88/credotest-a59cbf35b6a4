﻿$(document).ready(function () {
    LoadDropDown("Status");
    LoadDivWithResult();
});

function LoadDropDown(type) {
    $.ajax({
        url: "http://localhost:50397/api/" + type + "/" + $("#cmb" + type).val(),
        dataType: 'json',
        success: function(data) {
            var str = "";
            str += "<option id=0>";
            str += "All Tasks";
            str += "</option>";
            for (var i = 0; i < data["Table"].length; i++) {
                str += "<option id=" + data["Table"][i].Id + ">";
                str += data["Table"][i].Name;
                str += "</option>";
            }
            $("#cmb" + type).html(str);
        }
});
}

function LoadDivWithResult() {
    $.ajax({
        url: "http://localhost:50397/api/tasks/" + $("#cmbStatus").children(":selected").attr("id"),
        dataType: 'json',
        success: function(data) {
            var tableData = "<table id='ToDoTable' class='table table-condensed table-bordered table-responsive table-striped datatable'>";

            tableData += "<tr class='success'><td>Name</td>";
            tableData += "<td>Completed?</td></tr>";
            tableData += "<tr id='R0'><td><input id='txt0' type='text' class='form-control' onchange='AddUpdate(0, $(this).val(),\"\", null);'/></td>";
            tableData += "<td><input id='chk0' type='checkbox' onchange='AddUpdate(0, $(\"#txt0\").val(), $(this).val(), null);'/></td></tr>";
            $.each(data, function(i, item) {
                //tableData += "<tr><td id='" + item.Id + "'>" + item.Id + "</td>";
                var checked = false;
                if (item.Status === "completed" ) checked = true;
                tableData += "<tr>" +
                    "<td><input id='txt'" + item.Id + "' type='text' class='form-control' value='" + item.Name + "' onchange='AddUpdate(item.Id, $(this).val(),\"" + item.Status + "\", \"" + data + "\");'/></td>" +
                    "<td><input id='chk'" + item.Id + "' type='checkbox'  onchange='AddUpdate(item.Id, $(this).val(),\"" + item.Status + "\", \"" + data + "\");'" +
                    " checked='" + checked + "'/></td>" +
                    "</tr>";
            });
            tableData += "</tbody>" +
                "<tfoot>" +
                "<a onclick='DoAdd(\"ToDoTable\");'> Add </a></tfoot>" +
                "</table>";
            $("#tblResult").html(tableData);
        }
    });
}

function AddUpdate(taskId, name, status, data) {
    if (data == null) {
        $.ajax({
            url: "http://localhost:50397/api/tasks/" + name + "/" + status,
            type: "POST",
            success: function() {
                alert("Saved!!");
            }
        }).fail(function() {
            alert("Failed!!");
        });
    } else {
        $.ajax({
            url: "http://localhost:50397/api/tasks/" + taskId + "/" + name + "/" + status,
            type: "PUT",
            success: function() {
                alert("Saved!!");
            }
        }).fail(function () {
            alert("Failed!!");
        });
    }
}


function DoAdd(t) {
    var r0 = document.getElementById("R0");

    var r = r0.cloneNode(true);

    r.id = "R" + (r0.parentElement.rows.length + 5);
    r.style.display = "";
    if ($.contains($(r)[0].innerHTML, "R--") || $(r)[0].innerHTML.indexOf("R--") >= 0) {
        $(r).html($(r)[0].innerHTML.replace(/R--/g, r.id));
    }

    var find = '--0';
    $(r).html($(r)[0].innerHTML.replace(new RegExp(find, 'g'), '--' + r.id.substring(1, 9999)));

    $(r).children().each(function () {
        if (typeof ($(this).find("input").attr("onchange")) !== "undefined") {
            if ($(this).find("input").attr("onchange").indexOf('R0') >= 0) {
                $(this).find("input").attr("onchange", $(this).find("input").attr("onchange").replace('R0', r.id));
            }
        } else if (typeof ($(this).find("input").attr("onclick")) !== "undefined") {
            if ($(this).find("input").attr("onclick").indexOf('R0') >= 0) {
                $(this).find("input").attr("onclick", $(this).find("input").attr("onclick").replace('R0', r.id));
            }
        } else if (typeof ($(this).find("a").attr("onclick")) !== "undefined") {
            if ($(this).find("a").attr("onclick").indexOf('R0') >= 0) {
                $(this).find("a").attr("onclick", $(this).find("a").attr("onclick").replace('R0', r.id));
            }
        }
    });

    $(r).find("#RowID").val(r.id);
    r0.parentElement.appendChild(r);
    return false;
}