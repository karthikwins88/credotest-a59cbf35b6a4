﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ToDoApp.Models
{
    public class TasksModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
    }
}