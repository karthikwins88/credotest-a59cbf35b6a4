﻿<!DOCTYPE html>
<html>
<head>
    <title> Tasks Status Report</title>
    <script src="../Scripts/jquery-1.10.2.min.js"></script>
    <script src="../Scripts/TasksClientScripts.js"></script>
    <link href="../Content/bootstrap.css" rel="stylesheet" />
</head>
<body>
    <h2> Tasks Status Report</h2>
    <table class="table table-condensed table-bordered table-responsive table-striped datatable" style="width:500px">
        <tr style="text-align: left">
            <td> Status: &nbsp;
                <select id="cmbStatus" class="btn btn-primary dropdown-toggle" onchange="LoadDivWithResult();">
                </select>
            </td>
        </tr>
        <tr>
            <td>
                <div id="tblResult"></div>
            </td>
        </tr>
    </table>
</body>
</html>